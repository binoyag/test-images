FROM httpd:alpine

ARG VERSION="latest"

RUN echo -e "<html><body><h1>Testing App Version:${VERSION}</h1></body></html>" > /usr/local/apache2/htdocs/index.html
